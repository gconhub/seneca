const authenticateUser = (req,res,next) => {
    const userId = req.headers['x-user-id']

    if (userId === undefined) {
        res.status(403).send('Invalid undefined user!')
    }
    else if (!userId.match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/)) {
        res.status(403).send('Invalid format for userId')
    }
    else {
        next()
    }
}

module.exports = { authenticateUser }