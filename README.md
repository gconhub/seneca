# User stats post/get API

This is a mock API application for post and get user session stats.
Please refer to seneca-backend-swagger.yml for API interface documention.

NOTES

* This application use sessionId as the primary key in the data storage , it assumes that
every sessionId is unique. In case the client post the same sessionId multiple time, the
data will be overwritten by the last one ( taking that everything else including userId
and courseId match the existing entry on the DB ).

* The DB is sqlite for the purpose of being self contained application. Due to limitation of the library it require db to be open everytime when query, this is different from the real world where we
use pooling to keep/reuse dbconnection for the lifetime of the application without the overhead.

## Requirements

Required these packages to be installed in your environment

* node
* npm
* sqlite (optional, this is only to maintain the data in DB if needed)

Please install node on your machine by following the step on node official website (https://nodejs.org/). By default npm should come as part of node

If the installation was successful, you should be able to run the following command.

    $ node --version
    v12.18.2

    $ npm --version
    6.14.5

This application was developed using above node/npm versions. It is recommended to use the same or newer version if possible.

## Deployment and Install application

    $ ssh <AWS_HOST>
    $ git clone https://gitlab.com/gconhub/seneca.git
    $ cd seneca
    $ npm install

## Configure app

None

## Running the project

    $ npm start

The application will listen to http request on port 3000 by default

If you want to keep it running after closing it, use pm2

    $ npm install pm2 -g
    $ sudo pm2 start server.js

## Database

This application use sqlite as the database.
The sqlite.js is currently hard-coded to point to database name './data/seneca.db'
This is pre-built and should automatically work for you.

There are 2 tables in './data' directory, 
* seneca.db for main application
* seneca_test.db for the test

The application will automatically pick the db for you

### Create table 

You don't need to do this unless you accidentally delete it!

$ sqlite3 seneca.db
$ sqlite > CREATE TABLE user_stats(sessionId primary key,userId,courseId,totalModulesStudied,averageScore,timeStudied);

## Running test

npm test
