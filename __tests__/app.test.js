const request = require('supertest')
const app = require('../app.js')

describe('Test app', () => {
  it('handle home', async () => {
    const res = await request(app).get('/')

    expect(res.statusCode).toBe(200)
    expect(res.text).toBe('Welcome!')
  })

  it('handle 404', async () => {
    const res = await request(app).get('/xxx')

    expect(res.statusCode).toBe(404)
    expect(res.text).toBe('Not Found')
  })
})