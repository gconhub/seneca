const express = require('express')
const routes = require('./router')
const courses = require('./routes/course.js')
const app = express()

app.use('/courses', courses)
app.use(routes)

module.exports = app;