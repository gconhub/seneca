const { db } = require('../models/sqlite.js')

const storeUserSessionData = async (courseId, userId, statsDiff) => {

    var sql = `INSERT OR REPLACE into user_stats 
        (sessionId,userId,courseId,totalModulesStudied,averageScore,timeStudied) 
        values (?,?,?,?,?,?);`

    try {
        var dbConnection = await db()
        await dbConnection.run(
            sql,
            [
                statsDiff.sessionId,
                userId,
                courseId,
                statsDiff.totalModulesStudied,
                statsDiff.averageScore,
                statsDiff.timeStudied
            ]
        )
    }
    catch (error) {
        console.log(error)
        throw ('Failed to store user session data')
    }

}

const getUserData = async (courseId, userId, sessionId=undefined) => {

    var sql = 'SELECT * FROM user_stats WHERE courseId = ? AND userId = ?'
    var binds = [courseId, userId]
    if (sessionId !== undefined) {
        sql += ' AND sessionId = ?'
        binds.push(sessionId)
    }

    var rows = []
    try {
        var dbConnection = await db()
        rows = await dbConnection.all(sql, binds)
    }
    catch (error) {
        console.log(error)
        throw ('Failed to get user session data')
    }

    if (rows.length == 0) {
        return
    }

    var sumStudied = 0
    var sumScore = 0
    var sumTimeStudied = 0

    for (var i=0; i<rows.length; i++) {
        var row = rows[i]
        sumStudied += row.totalModulesStudied
        sumScore += row.averageScore
        sumTimeStudied += row.timeStudied
    }

    return {
        totalModulesStudied: sumStudied,
        averageScore: sumScore/rows.length,
        timeStudied: sumTimeStudied
    }

}

module.exports = { storeUserSessionData, getUserData }