var Validator = require('jsonschema').Validator;

/**
 * Validate payload embeded in the post request
 * If success, continue to the next step 
 * otherwise response back with HTTP CODE 400 with the list of errors
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
const validateCoursePayload = (req, res, next) => {
    var v = new Validator();

    var schema = {
        "id": "/SessionData",
        "type": "object",
        "properties": {
            "sessionId": {
                "type": "string",
                "pattern": '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'
            },
            "totalModulesStudied": {"type": "integer"},
            "averageScore": {"type": "float"},
            "timeStudied": {"type": "integer"},
        },
        "required": ["sessionId","totalModulesStudied","averageScore","timeStudied"]
    }

    v.addSchema(schema, '/SessionData');

    var result = v.validate(req.body, schema)
    var errors = result.errors
    if (errors.length > 0) {
        console.log(result)
        res.status('400').send(errors)
    }
    else {
        next()
    }
}

module.exports = { validateCoursePayload }