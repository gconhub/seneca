const request = require('supertest')
const app = require('../../app.js')
const { db } = require('../../models/sqlite.js')

const courseId = '2822cc6d-611a-4a8a-981d-c2698dd6a151'

describe('Test multiple post/sessions', () => {
  const userId = '82cd9451-3a61-4989-9308-8734961e2cb4'
  const userId2 = 'a15f9b4d-910c-44f7-8202-fab0773c1784'
  const sessionId1 = '73f4ec13-d2a3-4ccd-a782-f5fbd0e49f2f'
  const sessionId2 = '71f586c7-d7d7-4511-b127-43ecbd5786ee'
  const sessionId3 = '81146f12-7b7d-478e-8115-0acea0c78821'

  const body1 = {
    userId: userId,
    payload: {
      sessionId: sessionId1,
      totalModulesStudied: 10,
      averageScore: 99,
      timeStudied: 3600000
    }
  }

  const body2 = {
    userId: userId,
    payload: {
      sessionId: sessionId2,
      totalModulesStudied: 2,
      averageScore: 50,
      timeStudied: 1500000
    }
  }

  const body3 = {
    userId: userId2,
    payload: {
      sessionId: sessionId3,
      totalModulesStudied: 5,
      averageScore: 80,
      timeStudied: 1800000
    }
  }

  beforeAll(async (done) => {
    var dbConnection = await db()
    await dbConnection.run('DELETE FROM user_stats')
    done();
  });

  it('successfully post session data', async () => {
    var bodies = [body1, body2, body3]
    for (var i=0; i<bodies.length;i++) {
      var body = bodies[i]
      const res = await request(app)
      .post(`/courses/${courseId}`)
      .set({'X-User-Id': body.userId})
      .send(body.payload)
      
      expect(res.statusCode).toBe(201)
      expect(res.text).toBe('OK')
    }
  })

  it('successfully get userId1 session 1', async () => {
    const res = await request(app)
      .get(`/courses/${courseId}/sessions/${sessionId1}`)
      .set({'X-User-Id': userId})

    expect(res.statusCode).toBe(200)
    expect(res.body).toEqual({
      totalModulesStudied: 10,
      averageScore: 99,
      timeStudied: 3600000
    })
  })

  it('successfully get userId1 session 2', async () => {
    const res = await request(app)
      .get(`/courses/${courseId}/sessions/${sessionId2}`)
      .set({'X-User-Id': userId})

    expect(res.statusCode).toBe(200)
    expect(res.body).toEqual({
      totalModulesStudied: 2,
      averageScore: 50,
      timeStudied: 1500000
    })
  })

  it('successfully get userId2 session 3', async () => {
    const res = await request(app)
      .get(`/courses/${courseId}/sessions/${sessionId3}`)
      .set({'X-User-Id': userId2})

    expect(res.statusCode).toBe(200)
    expect(res.body).toEqual({
      totalModulesStudied: 5,
      averageScore: 80,
      timeStudied: 1800000
    })
  })

  it('successfully get userId1 aggregate data', async () => {
      const res = await request(app)
        .get(`/courses/${courseId}`)
        .set({'X-User-Id': userId})

      expect(res.statusCode).toBe(200)
      expect(res.body).toEqual({
        totalModulesStudied: 12,
        averageScore: 74.5,
        timeStudied: 5100000
      })
  })

  it('successfully get userId2 aggregate data', async () => {
    const res = await request(app)
      .get(`/courses/${courseId}`)
      .set({'X-User-Id': userId2})

    expect(res.statusCode).toBe(200)
    expect(res.body).toEqual({
      totalModulesStudied: 5,
      averageScore: 80,
      timeStudied: 1800000
    })
  })

  it('empty post request', async () => {
    const res = await request(app)
      .post(`/courses/${courseId}`)
      .set({'X-User-Id': body1.userId})
      .send({})
    
    expect(res.statusCode).toBe(400)
  })

  it('invalid post request', async () => {
    var body = body1
    body.payload.sessionId = 'Invalid uuid'
    const res = await request(app)
      .post(`/courses/${courseId}`)
      .set({'X-User-Id': body.userId})
      .send(body.payload)
    
    expect(res.statusCode).toBe(400)
  })

  it('invalid user return error', async () => {
    const res = await request(app)
    .get(`/courses/${courseId}`)
    .set({'X-User-Id': 'Invalid User'})

    expect(res.statusCode).toBe(403)
    expect(res.text).toBe('Invalid format for userId')
  })

  it('invalid user return error', async () => {
    const res = await request(app)
    .get(`/courses/${courseId}`)

    expect(res.statusCode).toBe(403)
    expect(res.text).toBe('Invalid undefined user!')
  })

  it('invalid course return error', async () => {
    const res = await request(app)
    .get(`/courses/123456`)
    .set({'X-User-Id': userId})

    expect(res.statusCode).toBe(404)
    expect(res.text).toBe('Not Found')
  })

  it('invalid session id return error', async () => {
      const res = await request(app)
      .get(`/courses/${courseId}/sessions/123456`)
      .set({'X-User-Id': userId})
  
      expect(res.statusCode).toBe(404)
      expect(res.text).toBe('Not Found')
  })

})