const { storeUserSessionData, getUserData } = require('../services/course.js')

// all header get automatically converted to lowercase
const getUserId = (req) => req.headers['x-user-id']

/**
 * Return the user course information that match the requested courseId, userId and/or sessionId
 * If the sessionId is provided, the data will be retrieved specific for that session
 * Otherwise, the aggregated value of the whole course for the user will be returned
 * 
 * Response is in the format of
 * {
        totalModulesStudied: <Integer>,
        averageScore: <Integer>,
        timeStudied: <Integer>
    }
 * @param {*} req 
 * @param {*} res 
 */
const getCourse = async (req,res) => {
    try {
        const userId = getUserId(req)

        var result = await getUserData(req.params.courseId, userId, req.params.sessionId)

        if (result === undefined) {
            res.status('404').send('Not Found')
        }
        else {
            res.status('200').send(result)
        }
    }
    catch (error) {
        console.log(error)
        res.status('400').send(error)
    }
}

/**
 * Store user course/session information
 * @param {*} req 
 * @param {*} res 
 */
const postCourse = async (req,res) => {
    try {
        const userId = getUserId(req)

        await storeUserSessionData(req.params.courseId, userId, req.body)

        res.status('201').send('OK')
    }
    catch (error) {
        console.log(error)
        res.status('400').send(error)
    }

}

module.exports = { getCourse, postCourse }