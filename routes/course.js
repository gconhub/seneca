const express = require('express')
const bodyParser = require('body-parser');
const router = express.Router()
const { getCourse, postCourse } = require('../controllers/course.js')
const { validateCoursePayload } = require('../validator/course.js')
const { authenticateUser } = require('../auth/user.js')

router.use(bodyParser.urlencoded({ extended: false}))
router.use(bodyParser.json())

// Home page
router.route('/').all((req, res, next) => {
    res.send('Welcome!')
})

// Authenticate the user for any request to /courses/*
router.all('/*', authenticateUser);

router.get('/:courseId', getCourse)
router.post('/:courseId', validateCoursePayload, postCourse)
router.get('/:courseId/sessions/:sessionId', getCourse)

// Fallback for non-supported route
router.route('*').all((req, res, next) => {
    res.status(404).send('Not Found')
})

module.exports = router