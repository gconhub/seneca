const Database = require('sqlite-async')

/**
 * Return database instance of sqlite database 'seneca.db'
 */
const db = () => {
    if ( process.env.NODE_ENV === 'test' ) { 
        return Database.open('./data/seneca_test.db')
    }
    else {
        return Database.open('./data/seneca.db')
    }
}

module.exports = { db }