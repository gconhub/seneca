const express = require('express')
const router = express.Router()

// Home page
router.route('/').all((req, res, next) => {
    res.send('Welcome!')
})

// Fallback for non-supported route
router.route('*').all((req, res, next) => {
    res.status(404).send('Not Found')
})

module.exports = router